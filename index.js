/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/
	function printUserInfo() {
	console.log("getUserInfo();");
	}
	printUserInfo();

	function getUserInfo() {
		let userInfo = {
			name: "AJ",
			age: 18,
			address: "#8 San Fernando, La Union",
			isMarried: false,
			petName: "Aldous"
		};
		return userInfo;
	}
	let myInfo = getUserInfo();
	console.log(myInfo);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/

	function printArtistsArray() {
	console.log("getArtistsArray();");
	}
	printArtistsArray();

	function getArtistsArray() {
	  let artistArray = ["5 Seconds of Summer", "Boys Like Girls", "Linkin park", "Simple Plan", "Sponge Cola"];
	  return artistArray;
	}
	let artistArray = getArtistsArray();
	console.log(artistArray);

/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function printSongsArray() {
	console.log("getSongsArray();");
	}
	printSongsArray();

	function getSongsArray() {
	  let songsArray = ["God's Not Dead", "Heaven is for Real", "Miracle from Heaven", "Do You Believe", "Breakthrough"];
	  return songsArray;
	}
	let songsArray = getSongsArray();
	console.log(songsArray);


/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function printMoviesArray() {
	console.log("getMoviesArray();");
	}
	printMoviesArray();

	function getMoviesArray() {
	  let moviesArray = ["Beside You", "Thunder", "New Divide", "Summer Paradise", "Dragonfly"];
	  return moviesArray;
	}
	let moviesArray = getMoviesArray();
	console.log(moviesArray);



/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/

	function printNumberArray() {
	console.log("getPrimeNumberArray();");
	}
	printNumberArray();

	function getPrimeNumberArray() {
	  let primeNumberArray = [2, 3, 5, 7, 11];
	  return primeNumberArray;
	}
	let primeNumberArray = getPrimeNumberArray();
	console.log(primeNumberArray);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}